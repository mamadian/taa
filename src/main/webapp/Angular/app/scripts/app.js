'use strict';

/**
 * @ngdoc overview
 * @name angularjsApp
 * @description
 * # angularjsApp
 *
 * Main module of the application.
 */
angular
  .module('angularjsApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/insertion', {
        templateUrl: 'views/insertion.html',
        controller: 'InsertionCtrl',
        controllerAs: 'insertion'
      })
      .when('/maj', {
        templateUrl: 'views/maj.html',
        controller: 'MajCtrl',
        controllerAs: 'maj'
      })
      .when('/liste', {
        templateUrl: 'views/liste.html',
        controller: 'ListeCtrl',
        controllerAs: 'liste'
      })
      .when('/suppression', {
        templateUrl: 'views/suppression.html',
        controller: 'SuppressionCtrl',
        controllerAs: 'suppression'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
