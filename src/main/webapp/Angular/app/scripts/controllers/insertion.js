'use strict';

/**
 * @ngdoc function
 * @name angularjsApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the angularjsApp
 */
angular.module('angularjsApp')
  .controller('InsertionCtrl', ['$scope','$http', function ($scope, $http) {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
    
    $scope.insertDeveloppeur = function(developper){
        $http.post("/developper", developper).success(function (data, status, headers,config) {
         $scope.action="developper créé avec success";
         $scope.error=false;
         $scope.show=true;
                })
                .error(function (data, status, header, config) {
                });
              };
                          
  }]);
