'use strict';

/**
 * @ngdoc function
 * @name angularjsApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the angularjsApp
 */
angular.module('angularjsApp')
  .controller('SuppressionCtrl',['$scope','$http','$routeParams','$location', function ($scope, $http ,$routeParams,$location) {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
    
    $scope.deleteDeveloppeur= function(developper){
        $http.delete("/developper/"+developper.id)
             .success(function (data, status, headers) {
                 $scope.action="supprimé";
                 $scope.show=true;
             })
             .error(function (data, status, header, config) {
             });
   }
}]);
