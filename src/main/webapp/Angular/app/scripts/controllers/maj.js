'use strict';

/**
 * @ngdoc function
 * @name angularjsApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the angularjsApp
 */
angular.module('angularjsApp')
  .controller('MajCtrl',['$scope','$http','$routeParams','$location', function($scope,$http,$routeParams,$location) {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
    
        $scope.updateDeveloppeur= function(developper){
            $http.put("/developper",developper).success(function(data, status, headers) {
              $scope.action="modifié";
              $scope.show=true;
            }).error(function(data, status, header, config) {
                });
          };
     
  }]);
