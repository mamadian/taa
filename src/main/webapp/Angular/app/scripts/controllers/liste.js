'use strict';

/**
 * @ngdoc function
 * @name angularjsApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the angularjsApp
 */
angular.module('angularjsApp')
  .controller('ListeCtrl', function ($scope, $http) {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
    $http.get("/developper").success(function(data) {
  		$scope.developpers = data;
    	}).error(function(data) {
  		$scope.developpers = [];
       });
  });
