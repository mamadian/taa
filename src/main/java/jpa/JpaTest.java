package jpa;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;
//import javax.persistence.criteria.*;
//import org.hibernate.criterion.CriteriaQuery;

import domain.*;

public class JpaTest {

    private EntityManager manager;

    public JpaTest(EntityManager manager) {
        this.manager = manager;
    }
   
	public static void main(String[] args)
	{
    	EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("prod");
        
        EntityManager entitymanager = emfactory.createEntityManager();
       
        //JpaTest test = new JpaTest(entitymanager);
        entitymanager.getTransaction().begin();
        try {
        	Priority P1 = new Priority("p1"); 
        	Status S1 = new Status("en cours");
        	Priority P2 = new Priority("p2"); 
        	Status S2 = new Status("termine");
        	//Developper dev2= new Developper("dev2");
        	//entitymanager.persist(dev2);
        	entitymanager.persist(P1);
	        entitymanager.persist(S1);
	        entitymanager.persist(P2);
	        entitymanager.persist(S2);
	        int numTask= entitymanager.createQuery("SELECT t FROM Task t",Task.class).getResultList().size();
        	if(numTask==0){
        		Developper dev1= new Developper("dev1");
            	Developper dev2= new Developper("dev2");
            	Task t1=new Task("t1",S1,P1);
            	Task t2=new Task("t2",S2,P1);
            	/*Task t3=new Task("t3",S1,P2);
            	Task t4=new Task("t4",S2,P2);*/
				dev1.addTask(t1);
				dev1.getTasks().add(t2);

				//Sprint sp1=new Sprint();
//				sp1.getTaches().add(t1);


            	entitymanager.persist(dev2);
    	       /* entitymanager.persist(t3);
    	        entitymanager.persist(t4);*/
    	        entitymanager.persist(dev1);
    	        entitymanager.persist(t1);
    	        entitymanager.persist(t2);

        	}
        	//liste des developpers qui ont des taches
        	List<Developper> listeDevelopper= entitymanager.createQuery("Select a From Developper a", Developper.class).getResultList();
        	System.out.println("liste ");
        	
        	System.out.println("num of Developper:" + listeDevelopper.size());
            for (Developper next : listeDevelopper) {
                System.out.println("next Developper: " + next.getName());
            }
        	
        	//liste de tous les  developpers avec les criteria 
           /* CriteriaBuilder cb = entitymanager.getCriteriaBuilder();
            javax.persistence.criteria.CriteriaQuery<Task> q=  cb.createQuery(Task.class);
           Root<Task> c = (Root<Task>) q.from(Task.class);
          //  ((javax.persistence.criteria.CriteriaQuery<Task>) q).select(c);

			TypedQuery<Task> typedQuery = entitymanager.createQuery((javax.persistence.criteria.CriteriaQuery<Task>) c);
			List<Task> resultList = typedQuery.getResultList();

        	System.out.println("liste des taches:");
        	System.out.println("num of Task:" + resultList.size());
            for (Task next : resultList) {
                System.out.println("next Task: " + next.getName());
            }
        	*/
        	
            //liste des developpers qui ont des taches de priorité P1
            System.out.println("liste des developpers de priorite 1");
        	List<Developper> listeDevelopper2= entitymanager.createQuery("Select d From Developper d, Task t ,Priority p Where d.id=t.id AND t.id=p.id AND p.id=1 ").getResultList();
        	System.out.println("num of Developper:" + listeDevelopper2.size());
            for (Developper next : listeDevelopper2) {
                System.out.println("next Developper: " + next.getName());
            }
            
            //liste des taches du developper 1
            System.out.println("liste des taches du developpeur 1");
        	List<Task> listeTache1= entitymanager.createQuery("Select t From Task t,Developper d ").getResultList();
        	System.out.println("num of Task:" + listeTache1.size());
            for (Task next : listeTache1) {
                System.out.println("next Task: " + next.getName()+" "+next.getPriority().getName()+" "+ next.getStatus().getName());
            }
        	/*CriteriaBuilder criteriaBuilder = entitymanager.getCriteriaBuilder();
        	CriteriaQuery<Task.class> query = criteriaBuilder.createQuery(Task.class);
        	Root<Task> from = query.from(Task.class);*/
        } catch (Exception e) {
            e.printStackTrace();
        }
        entitymanager.getTransaction().commit();
        entitymanager.close();
        emfactory.close();
        System.out.println(".. done");
    }
    
}
