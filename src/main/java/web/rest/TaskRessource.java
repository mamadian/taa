package web.rest;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import io.swagger.annotations.Api;
import repository.TaskRepository;
import domain.Task;

@Path("/task")
@Api(value="/task", description = "task resource")
public class TaskRessource {
	
	private TaskRepository taskHistory = new TaskRepository();
	
	@GET
    @Produces(MediaType.APPLICATION_JSON)
	public List<Task> getAllTask(){
		return taskHistory.findAllTask();
	}
}
