package web.rest;


import java.util.List;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.cxf.rs.security.cors.CrossOriginResourceSharing;

import domain.Developper;
import io.swagger.annotations.Api;
import repository.DevelopperRepository;

@Path("/developper")
@Api(value="/developper", description = "Developper resource")
@CrossOriginResourceSharing
public class DevelopperRessource {
	
	private DevelopperRepository developpeRepository = new DevelopperRepository();
	

    @GET
    @Produces(MediaType.APPLICATION_JSON)
	public List<Developper> getAll()
	{
		return developpeRepository.findAll();
	}
    
    @CrossOriginResourceSharing
	@POST
	@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN})
	@Produces({MediaType.APPLICATION_JSON,MediaType.TEXT_PLAIN})
	public Response createDevelopper(Developper developper)
	{
		 developpeRepository.insertDevelopper(developper);
		 System.out.println("je suis par la");
		 return Response.status(201)
			        .entity("A new author resource has been created")
			        .header("Access-Control-Allow-Headers", "X-extra-header")
			        .header("Access-Control-Allow-Credentials", "true")
			        .header("Access-Control-Allow-Origin", "*")
			        .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
			        .header("Access-Control-Allow-Headers", "X-Requested-With, Content-Type, X-Codingpedia")
			        .allow("OPTIONS").build();
	}

	@DELETE
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public  void  deleteDevelopper(@PathParam("id") String id)
	{
		developpeRepository.removeDevelopper(Long.parseLong(id));

	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Developper updateDevelopper(Developper developper)
	{
		return developpeRepository.updateDevelopper(developper);
	}
}
