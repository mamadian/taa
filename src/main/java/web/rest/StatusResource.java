package web.rest;


import domain.Status;
import io.swagger.annotations.Api;
import repository.StatusRepository;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * Created by masoyc on 24/11/2015.
 */
@Path("/status")
@Api(value="/status", description = "Status resource")
public class StatusResource
{
    private StatusRepository statusRepositoryRepository = new StatusRepository();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Status> getAll()
    {
        return statusRepositoryRepository.findAll();
    }
}
