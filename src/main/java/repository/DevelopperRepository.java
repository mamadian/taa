package repository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import domain.Developper;
import domain.Developper_web;


public class DevelopperRepository {
	EntityManagerFactory factory = Persistence.createEntityManagerFactory("dev1");
    EntityManager manager = factory.createEntityManager();
    
    public List<Developper> findAll(){
    	EntityTransaction tx = manager.getTransaction();
        tx.begin();
        List<Developper> departments = new ArrayList<Developper>();
        try {
            departments = manager.createQuery("SELECT d from Developper d").getResultList();

        } catch (Exception e) {
            e.printStackTrace();
        }
        tx.commit();
        manager.close();
        factory.close();

        System.out.println("..done1");
        return departments;

    }

    public Developper insertDevelopper(Developper developper)
    {
        EntityTransaction tx = manager.getTransaction();
        tx.begin();
        manager.persist(developper);
        tx.commit();
        manager.close();
        factory.close();
        return developper;
    }

    public Developper updateDevelopper(Developper developper)
    {
        EntityTransaction tx = manager.getTransaction();
        tx.begin();
        manager.merge(developper);
        tx.commit();
        manager.close();
        factory.close();
        return developper;
    }

    public void removeDevelopper(long id)
    {
        EntityTransaction tx = manager.getTransaction();
        tx.begin();
        Developper developper=manager.find(Developper.class,id);
        manager.remove(developper);
        tx.commit();
        manager.close();
        factory.close();

    }
}


