package repository;

import domain.Department;
import domain.Employee;
import domain.Status;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by masoyc on 24/11/2015.
 */
public class StatusRepository
{
    EntityManagerFactory factory = Persistence.createEntityManagerFactory("dev1");
    EntityManager manager = factory.createEntityManager();

    public List<Status> findAll()
    {
        EntityTransaction tx = manager.getTransaction();
        tx.begin();
        List<Status> status = new ArrayList<>();
        try
        {
            status = manager.createQuery("SELECT d from Status d", Status.class).getResultList();

        } catch (Exception e) {
            e.printStackTrace();
        }
        tx.commit();
        manager.close();
        factory.close();
       /* for(Status dep:status)
        {
            System.out.println(dep.getName());
        }*/
        return status;
    }

    public static void main(String[] args) {

        StatusRepository rep=new StatusRepository();
        List<Status>status=rep.findAll();

        for(Status s:status)
        {
            System.out.println(s.getName());
        }

    }


}
