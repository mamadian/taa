package repository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import domain.Priority;
import domain.Status;
import domain.Task;


public class TaskRepository {
	EntityManagerFactory factory = Persistence.createEntityManagerFactory("dev1");
    EntityManager manager = factory.createEntityManager();
    
    public List<Task> findAllTask() {
        EntityTransaction tx = manager.getTransaction();
        tx.begin();
        List<Task> tasks = new ArrayList<Task>();
        try {
            tasks = manager.createQuery("SELECT t from Task t").getResultList();

        } catch (Exception e) {
            e.printStackTrace();
        }
        tx.commit();
        manager.close();
        factory.close();

        return tasks;
    }
    
    public List<Task> findTaskbyDevelopper(Long id_developper){
    	EntityTransaction tx = manager.getTransaction();
        tx.begin();
        List<Task> tasks = new ArrayList<Task>();
        try {
			tasks= manager.createQuery("SELECT t from Task t ").getResultList();
		} catch (Exception e) {
			 e.printStackTrace();
		}
        tx.commit();
        manager.close();
        factory.close();
        return tasks;
    }
    public List<Task> findUserHistory(Long id_developper){
    	EntityTransaction tx = manager.getTransaction();
        tx.begin();
        List<Task> tasks = new ArrayList<Task>();
        try {
        	String query="SELECT t from Task t Status s Developper d Where t.id=s.id AND d.id=s.id AND d.id=:id_developper AND s.id=1";
			tasks= manager.createQuery(query).getResultList();
		} catch (Exception e) {
			 e.printStackTrace();
		}
        tx.commit();
        manager.close();
        factory.close();
        return tasks;
    }
    
    public void InsertTask(){
    	EntityTransaction tx = manager.getTransaction();
        tx.begin();
        try {
        	Priority P1 = new Priority("p1"); 
        	Status S1 = new Status("en cours");
        	manager.persist(P1);
	        manager.persist(S1);
		} catch (Exception e) {
			
		}
        tx.commit();
        manager.close();
        factory.close();
    }
    
}
