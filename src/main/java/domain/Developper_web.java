package domain;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("web")
public class Developper_web extends Developper{
	String logiciel;

	public Developper_web() {
		super();
	}

	public Developper_web(String name, String logiciel) {
		super(name);
		this.logiciel=logiciel;
	}
	
}
