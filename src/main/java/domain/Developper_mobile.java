package domain;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("mobile")
public class Developper_mobile extends Developper{
	String type;

	public Developper_mobile() {
		super();
	}

	public Developper_mobile(String name, String type) {
		super(name);
		this.type=type;
	}
	
}
