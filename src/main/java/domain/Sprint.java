package domain;

import java.util.List;

import javax.persistence.*;

@Entity
public class Sprint {
	
	private Long id;
	private String name;
    List<Task> task;
    
    public Sprint(String name, List<Task> task) {
		super();
		this.name = name;
		this.task = task;
	}
    
    public Sprint() {
		super();
	}

	@Id
	@GeneratedValue
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@OneToMany
	public List<Task> getTaches() {
		return task;
	}
	public void setTaches(List<Task> taches) {
		this.task = task;
	}
	
    
}
