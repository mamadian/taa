package domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Status {
	
	@Id
	@GeneratedValue
	private Long Id;
	
	private String name;
	
	public Status(){
		super();
	}
	public Status(String name) {
		super();
		this.name = name;
	}
	
	public Long getId() {
		return Id;
	}
	
	public void setId(Long id) {
		Id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
