package domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

@Inheritance
@Entity
@DiscriminatorColumn(name = "Dev_type")
public class Developper {
	

	 private Long id;
	 private String name;
	 private List<Task>tasks;
	 
	 public Developper()
	 {
			super();
			// TODO Auto-generated constructor stub
		 tasks=new ArrayList<Task>();
		}
	 
	 public Developper(String name) {
			this.name = name;
		 tasks=new ArrayList<Task>();
	 }

	@Id
	@GeneratedValue
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@OneToMany(cascade = CascadeType.PERSIST,fetch = FetchType.EAGER)
	public List<Task> getTasks() {
		return tasks;
	}

	public void setTasks(List<Task> tasks) {
		this.tasks = tasks;
	}

	public  void addTask(Task t)
	{
		getTasks().add(t);

	}
}
	
	 
	 

