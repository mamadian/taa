# # PROJET TAA ET GLI # #

Ce README fait un petit résumé de ce qu'on a pu réaliser sur le projet **TAA** ET **GLI** . Il explique également comment exécuter les différents composants.

### Présentation du projet ###

L'objectif du projet est de construire une application de gestion de développement en mode Agile, dans lequel on pourra créer des tâches et des développeurs puis les associer. Une architecture de notre projet se trouve [ici](https://bitbucket.org/mamadian/taa/src/5e041e88909031c451a8c8487ad50f38f53c7a5f/Architecture%20Technique%20TAA-GLI.pdf?fileviewer=file-view-default).

## Partie TAA ##

***Etape 1***
Concevoir le modèle métier du projet et utiliser JPA pour persister les données dans une base de données soit en local ou une base de données dans d'autre serveur.
Le modele contient:

* des developpeurs
* deux types de développeur : développeur web et développeur mobile
* des tâches
* chaque développeur peut avoir plusieurs tâches 
* des priorités : par exemple <<immediate >>
* des status : par exemple <<en cours>>
* une tâche a une priorité et un status 

Le lien vers notre modèle JPA se trouve [ici](https://bitbucket.org/mamadian/taa/src/6268d4fa15952351a102dbd5c0a8ffb6a047be5d/src/main/java/domain/).

Le repository se trouve [ici](https://bitbucket.org/mamadian/taa/src/6268d4fa15952351a102dbd5c0a8ffb6a047be5d/src/main/java/repository/)

***Etape 2*** 
Développer un service web REST qui permettra qui va interagir avec la couche métier déjà fait

* création des ressources avec les annotations JAXRS
* Integration d'un swagger avec notre application afin de pouvoir tester facilement dans une page web

Le lien vers notre service Rest se trouve [ici](https://bitbucket.org/mamadian/taa/src/6268d4fa15952351a102dbd5c0a8ffb6a047be5d/src/main/java/web/rest/).

***Etape 3***

En utilisant Spring Aop ajoutez un aspect permettant de logger tous les accès au service Rest. Afin de pouvoir toutes etapes du projet on n'a un service que pour le développeur.

Le service SPRING AOP se trouve [ici](https://bitbucket.org/mamadian/taa/src/6268d4fa15952351a102dbd5c0a8ffb6a047be5d/src/main/java/aspect/).

Le fichier de configuration spring se trouve à la racine.

***Etape 4***  

Mise en place de Docker

* les fichiers pour la mise en place de docker se trouvent sur la racine.

## Partie GLI ##

Faire deux interfaces en utilisant respectivement  **AngularJs** et **GWT** pour notre application.

*Angularjs: cette partie se trouve sur un dossier Angular du répertoire webapp 
*GWT : la partie GWT on la rendu sur projet a part 

### Comment exécuter L'application ? ###

Pour executer le projet il faut : 

* se positioner sur le repertoire racine du projet
* lancer la base de donnée avec ces deux scripts en ligne de commande

```
#!shell

run-hsqldb-server.sh

show-hsqldb.sh

```

* lancer le serveur Rest: en executant la classe java RestApplication.java

A partir de là on peut deja voir le service avec le swagger sur le port https://localhost:8081

* Pour voir la partie Angular se positionner dans le répertoire Angular et executer grunt serve en lige de commande.

### binôme ###
 Diallo Mamadou Dian && Yvan Maso Christien